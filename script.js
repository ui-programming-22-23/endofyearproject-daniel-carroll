  //INIT
  //Link documents
  const canvas = document.getElementById('canvas');
  const player = document.getElementById('player');
  const hitBox1 = document.getElementById('HitBox1');

  const myButton = document.getElementById("myButton"); 
  
  //Set player starting pos
  let playerTop = 550;
  let playerLeft = 25;
  let playerDirection = 1;
  
  //Set initial night to 0
  let currentNight = 0;
  console.log(currentNight);

  //Set canMove to false by default
  //let canMove = true;
  // Add an event listener to listen for keypress events

  timer();
  gameStates();



///GAME STATE
function gameStates()
{
  //Menu before game
  if(currentNight == 0)
  {
    console.log("Night 0 if statement working");
  }

  //Night 1 npc dialogue 
  if(currentNight == 1)
  {
    move();
    collision();//Checks if player can press e to chat to npcs
    console.log("Night 1 if statement working");
  }

  //Night 2 npc dialogue 
  if(currentNight == 2)
  {
    move();
    collision();//Checks if player can press e to chat to npcs
    console.log("Night 2 if statement working");
  }

  //Night 3 npc dialogue 
  if(currentNight == 3)
  {
    move();
    collision();//Checks if player can press e to chat to npcs
    console.log("Night 3 if statement working");
  }

  //Decision night
  if(currentNight == 4)
  {
    console.log("Night 4 if statement working");
  }

  requestAnimationFrame(gameStates);//GameLoop
}



// Define the function to move the player character
function move(event) {

  document.addEventListener('keydown', move); 
  
  // Move the player character to the left when the 'a' key is pressed
  if (event.key === 'a') {
    playerLeft -= 10; // Move 10 pixels to the left
    player.style.left = playerLeft + 'px'; // Update the position of the player character
    playerDirection = -1;
    player.style.transform = 'scaleX(' + playerDirection + ')';

  }
  else if (event.key === 'd') // Move the player character to the right when the 'd' key is pressed
  {
    playerLeft += 10; // Move 10 pixels to the right
    player.style.left = playerLeft + 'px'; // Update the position of the player character
    playerDirection = 1;
    player.style.transform = 'scaleX(' + playerDirection + ')';
  }

  //border checking
  if(playerLeft <= 0)
  {
    playerLeft = 10;
  }
  else if(playerLeft >= 1220)
  {
    playerLeft = 1220;
  }
}


//Timer
function timer()
{
// Create timer element
const timer = document.createElement("div");
timer.id = "timer";
canvas.appendChild(timer);

//Create night element
const night = document.createElement("div");
night.id = "night";
night.innerText = "Night 0";
canvas.appendChild(night);

//Set initial time to 3 minutes (in seconds)
let timeLeft = 3 * 10;

//Update timer every second
setInterval(() => {
  //Update timer text
  timer.innerText = formatTime(timeLeft);

  //Decrement time left
  timeLeft--;

  //End timer when time runs out
  if (timeLeft < 0) {
    //Increment night and reset time
    currentNight++;
    timeLeft = 3 * 10;

    // Update night text
    night.innerText = `Night ${currentNight}`;

    //Reset player pos
    playerLeft = 25;
    player.style.left = playerLeft + 'px';
    playerDirection = 1;
    player.style.transform = 'scaleX(' + playerDirection + ')';
  }
}, 1000);
}

//Function to format time as m:ss
function formatTime(time) {
  const minutes = Math.floor(time / 60);
  const seconds = time % 60;
  return `${minutes.toString().padStart(2, "")}:${seconds.toString().padStart(2, "0")}`;
}


//Function for player npc dialouge collision
function collision()
{
  if(playerLeft > 180 && playerLeft < 280)
  {
    playerLeft = 1000; // Move 10 pixels to the right
    player.style.left = playerLeft + 'px'; // Update the position of the player character
  }
}

//Skip night button
  myButton.addEventListener("click", function() 
  {
    //Increment night and reset time
    currentNight++;
    timeLeft = 3 * 10;

    // Update night text
    night.innerText = "Night" + currentNight;

    //Reset player pos
    playerLeft = 25;
    player.style.left = playerLeft + 'px';
    playerDirection = 1;
    player.style.transform = 'scaleX(' + playerDirection + ')';
  });